package com.tc.itfarm.service.impl;

import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.api.util.BeanMapper;
import com.tc.itfarm.api.util.DateUtils;
import com.tc.itfarm.dao.MenuArctileDao;
import com.tc.itfarm.model.MenuArctile;
import com.tc.itfarm.model.MenuArctileCriteria;
import com.tc.itfarm.service.MenuArticleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2016/9/5.
 */
@Service
public class MenuArctileServiceImpl extends BaseServiceImpl<MenuArctile> implements MenuArticleService {

    @Resource
    private MenuArctileDao menuArctileDao;

    @Override
    protected SingleTableDao getSingleDao() {
        return menuArctileDao;
    }

    @Override
    public List<Integer> selectArticleByMenuId(Integer menuId, Page page) {
        MenuArctileCriteria criteria = new MenuArctileCriteria();
        criteria.or().andMenuIdEqualTo(menuId);
        if (page != null) {
            int count = menuArctileDao.countByCriteria(criteria);
            page.setTotalRecords(count);
            criteria.setPage(page);
        }
        List<MenuArctile> menuArctiles = menuArctileDao.selectByCriteria(criteria);
        return BeanMapper.copyPropertyToList(menuArctiles, Integer.class, "articleId");
    }

    @Override
    public void deleteByArticleId(Integer articleId) {
        MenuArctileCriteria criteria = new MenuArctileCriteria();
        criteria.or().andArticleIdEqualTo(articleId);
        menuArctileDao.deleteByCriteria(criteria);
    }

    @Override
    public void updateArticleIds(Integer menuId, Integer[] ids) {
        MenuArctileCriteria criteria = new MenuArctileCriteria();
        criteria.or().andMenuIdEqualTo(menuId);
        menuArctileDao.deleteByCriteria(criteria);
        for (Integer id : ids) {
            MenuArctile ma = new MenuArctile();
            ma.setMenuId(menuId);
            ma.setCreateTime(DateUtils.now());
            ma.setArticleId(id);
            menuArctileDao.insert(ma);
        }
    }
}
