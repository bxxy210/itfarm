package com.tc.itfarm.dao;

import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.model.Menu;
import com.tc.itfarm.model.MenuCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MenuDao extends SingleTableDao<Menu, MenuCriteria> {
    Integer selectMaxOrder(Integer parentId);
}