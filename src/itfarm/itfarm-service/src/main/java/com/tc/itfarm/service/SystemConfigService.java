package com.tc.itfarm.service;

import com.tc.itfarm.model.SystemConfig;

/**
 * Created by wangdongdong on 2016/8/29.
 */
public interface SystemConfigService extends BaseService<SystemConfig> {
}
