<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
<div id="dcWrap">
    <jsp:include page="../header.jsp"></jsp:include>
    <div id="dcMain">
        <!-- 当前位置 -->
        <!-- 当前位置 -->
        <div id="urHere">DouPHP 管理中心<b>></b><strong>文章列表</strong></div>
        <div class="mainBox" style="height:auto!important;height:550px;min-height:550px;">
            <h3><a href="${ctx }/article/addArticleUI.do" class="actionBtn add">添加文章</a>文章列表</h3>
            <div class="filter">
                <form action="${ctx }/article/articleList.do" method="get">
                    <select name="typeId">
                        <option value="0">未分类</option>
                        <c:forEach items="${categorys }" var="item">
                            <option value="${item.recordId }"
                                    <c:if test="${typeId==item.recordId }">selected='selected'</c:if>> ${item.name }</option>
                        </c:forEach>
                    </select>
                    <input name="title" type="text" class="inpMain" value="${title }" size="20"/>
                    <input name="submit" class="btnGray" type="submit" value="筛选"/>
                </form>
            </div>
            <div id="list">
                <form name="action" method="post" action="${ctx}/article/bacth.do">
                    <table width="100%" border="0" cellpadding="8" cellspacing="0" class="tableBasic">
                        <tr>
                            <th width="22" align="center"><input name='chkall' type='checkbox' id='chkall'
                                                                 onclick='selectcheckbox(this.form)' value='check'></th>
                            <th width="40" align="center">编号</th>
                            <th align="left">文章名称</th>
                            <th width="150" align="center">文章分类</th>
                            <th width="80" align="center">最后修改日期</th>
                            <th width="80" align="center">添加人</th>
                            <th width="80" align="center">操作</th>
                        </tr>
                        <c:forEach items="${articles }" var="item" varStatus="vs">
                            <tr>
                                <td align="center"><input type="checkbox" name="id" value="${item.article.recordId}"/></td>
                                <td align="center">${vs.count }</td>
                                <td><a href="${ctx }/article/editArticleUI.do?recordId=${item.article.recordId}">${item.article.title }</a></td>
                                <td align="center">${item.categoryName }</td>
                                <td align="center">${item.lastDate }</td>
                                <td align="center">${item.authorName }</td>
                                <td align="center">
                                    <a href="${ctx }/article/editArticleUI.do?recordId=${item.article.recordId}">编辑</a>
                                    | <a href="${ctx }/article/delete.do?recordId=${item.article.recordId}">删除</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                    <div class="action">
                        <select name="action" onchange="doAction()">
                            <option value="0">请选择...</option>
                            <option value="del_all">删除</option>
                            <option value="category_move">移动分类至</option>
                        </select>
                        <select name="new_cat_id" style="display:none">
                            <option value="-1">未分类</option>
                            <c:forEach items="${categorys }" var="item">
                                <option value="${item.recordId }"> ${item.name }</option>
                            </c:forEach>
                        </select>
                        <input name="submit" class="btn" onclick="action()" type="submit" value="执行"/>
                    </div>
                </form>
                <itfarm:PageBar pageUrl="/article/articleList.do?typeId=${typeId }&title=${title }"
                                pageAttrKey="page"></itfarm:PageBar>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <script type="text/javascript">

        onload = function () {
            document.forms['action'].reset();
        }

        function doAction() {
            var frm = document.forms['action'];

            frm.elements['new_cat_id'].style.display = frm.elements['action'].value == 'category_move' ? '' : 'none';
        }

    </script>
    <jsp:include page="../footer.jsp"></jsp:include>
</body>
</html>